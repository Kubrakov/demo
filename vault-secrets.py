import hvac
import sys
import requests
client = hvac.Client(url='http://1.2.3.4:80', token=sys.argv[3])
mount_point = sys.argv[1]
secret_path = sys.argv[2]

read_secret_result = client.secrets.kv.v1.read_secret(
    path=secret_path,
    mount_point=mount_point,
)
keys = read_secret_result['data']
envs = ""
for key, value in keys.items():
    envs = envs + key + ": " + value + "\n"
print (envs)
